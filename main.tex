\RequirePackage{luatex85}
\documentclass[Ligatures=TeX,table,svgnames,usetotalslideindicator,compress,10pt]{beamer}

\usepackage{polyglossia}

\setdefaultlanguage{english}
\disablehyphenation
\usetheme{metropolis}
\usepackage{chemformula}
\setbeamertemplate{section in toc}[sections numbered]

\usepackage[style=verbose,backend=biber]{biblatex}
\usepackage{multimedia}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{amssymb}
\usepackage{hyperref}
\addbibresource{ref.bib}
\usepackage{appendixnumberbeamer}

\title{Long-term evaluation for sizing low-carbon cloud data centers}

\subtitle{COMPAS 2023 @ Annecy}

\author{\textbf{Miguel Vasconcelos}$^{1,2}$, Daniel Cordeiro$^{1}$, Georges Da Costa$^{3}$, Fanny Dufossé$^{2}$, Jean-Marc Nicod$^{4}$ and Veronika Rehn-Sonigo$^{4}$}
\institute
{  
  School of Sciences, Arts, and Humanities, University of São Paulo, Brazil$^{1}$\\
  Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, Grenoble, France$^{2}$\\
  IRIT, Université de Toulouse, CNRS UMR 5505, Toulouse, France$^{3}$\\
  FEMTO-ST Institute, Univ. Bourgogne Franche-Comté, CNRS, ENSMM, Besan\c con, France$^{4}$\\ 
}

\date{July 2023}

\begin{document}

\renewcommand{\footnotesize}{\scriptsize}

\frame{\titlepage}

\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents[]
\end{frame}

\section{Introduction}

\begin{frame}{Environmental impact of the clouds}

  \begin{itemize}
  \item  Data Centers (DCs): \alert{$\simeq 1$\%} of global electricity in 2021 (IEA)
  \item  Cloud providers are integrating renewable in their operations
  \item \alert{Environmental impact of manufacturing} the renewable infrastructure and the servers
    \pause

    \begin{columns}[T]
      \begin{column}{0.5\textwidth}
        \begin{center}
          \begin{figure}[!h]
            \centering
            \includegraphics[width=.8\textwidth]{images/google_region_picker.jpeg}
            \caption{Google's region picker tool.}
          \end{figure}
        \end{center}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{center}
          \begin{figure}[!h]
            \centering
            \includegraphics[width=.75\textwidth]{images/fb_co2.png}      
            \caption{Facebook carbon footprint. Opex refers to the DC's
              recurring operations, and Capex to the one-time infrastructure
              and hardware.\footnotemark[1]}
            
          \end{figure}
        \end{center}
      \end{column}
    \end{columns}

  \end{itemize}
  \footnotetext[1]{\fullcite{gupta2020_chasingcarbon}}

\end{frame}

\begin{frame}{Previous work}
  \alert{Linear program (LP) formulation to minimize the cloud federation operation's carbon emissions} (timespan of 1 year, 1 h time slots)\footnotemark[2]
  
  \begin{itemize}
    
  \item \alert{Scheduling} of tasks and \alert{dimensioning} of
    renewable infrastructure modeled as single problem
    \begin{itemize}      
    \item Allocate workload to other DC vs add more renewables
    \end{itemize}
  \item Only real variables : \alert{Optimal} solution in \alert{polynomial time}
  \item \alert{Flexibility of the model} to evaluate other scenarios
    \begin{itemize}
    \item  workload, locations (solar irradiation, grid electricity mix and \ch{CO2} emissions, cooling needs), servers, ...
      
    \end{itemize}
    
  \end{itemize}
  \footnotetext[2]{\fullcite{ccgrid2023_lowcarbon}}
\end{frame}


\begin{frame}{Assumptions}


  \textbf{Data centers:}

  \begin{itemize}

  \item IT Infrastructure already built (servers, network)
  \item Homogeneous (regarding CPU cores)
  \item Server power consumption: idle and dynamic 
  \item Intra network power consumption: static
  \item Geographically distributed
  \item Power Usage Effectiveness (PUE) for each DC

  \end{itemize}

  \begin{figure}[!h]
    
    \centering

    \setlength\abovecaptionskip{-3pt}
    
    \includegraphics[width=0.6\textwidth]{images/locations_.pdf}
    \caption{Selected data centers location (inspired from Microsoft Azure)}

  \end{figure}  
\end{frame}

\begin{frame}{Assumptions}  
  \textbf{Workload:}
  
  \begin{itemize}  
  \item All tasks must be scheduled and executed on time
  \item Task execution cannot be delayed
  \item No migration 
  \item Batch tasks that can be executed in any of the DCs 

  \end{itemize}

\end{frame}

\begin{frame}{Assumptions}  
  \textbf{Renewable infrastructure:}
  
  \begin{itemize}    
  \item Batteries charge and discharge efficiency (85\%), Maximum
    Depth of Discharge (20\%)
  \item Solar panels (PVs) efficiency (15\%)
  \item Solar irradiation at each location (MERRA-2)
  \item Carbon emissions from manufacturing PVs (250 kg \ch{CO2} eq
    per m²) and batteries (59 kg \ch{CO2} eq per kWh)
  \item Lifetime (PV: 30 years, battery: 10 years)  
    
  \end{itemize}
  
\end{frame}

\begin{frame}{Assumptions}  
  \textbf{Local electricity grid:}
  
  \begin{itemize}
    
  \item The energy mix is different at each location
  \item May have the presence of  low carbon-intensive sources    
    
    \begin{table}
      
      \caption{Emissions (in g \ch{CO2}-eq/kWh) for using the regular grid. Source for grid emissions: electricityMap, climate-transparency.org.}\label{tab:carbonfootprint} \centering

      % \footnotesize
      \begin{tabular}{|l|r|}
        
        \hline
        \textbf{Location} &  \textbf{Emissions }  \\
        \hline
        Johannesburg & 900.6  \\
        \hline
        Pune & 702.8\\
        \hline
        Canberra & 667.0 \\
        \hline
        Dubai & 530.0   \\
        \hline
        Singapore & 495.0  \\
        \hline     
        Seoul & 415.6  \\
        \hline
        Virginia  & 342.8  \\
        \hline
        São Paulo &  61.7 \\
        \hline 
        Paris &  52.6  \\
        \hline  

      \end{tabular}  
    \end{table}
    
  \end{itemize}
  \normalsize
\end{frame}


\begin{frame}{  Linear Program summary}

  \alert{Obj. function:} Minimize the DC's operation \ch{CO2} emissions (1 year)
  \begin{itemize}
  \item \ch{CO2} comes from: grid power, manufacturing PV and batteries
  \end{itemize}
  
  \begin{columns}[T]
    \begin{column}{0.55\textwidth}
      \pause
      \alert{Input}

      \begin{itemize}
      \item Power consumption of servers and network equipment
      \item 1 year of workload  (Google trace)
      \item Renewable infrastructure specs (efficiency, manufacturing \ch{CO2})
      \item For each DC:
        \begin{itemize}
        \item Solar irradiation (1 year)
        \item \ch{CO2} from electricity grid
        \item PUE       
        \item CPU cores number
        \end{itemize}
      \end{itemize}
      
    \end{column}

    \begin{column}{0.45\textwidth}
      
      \pause          
      \alert{Output}
      
      \begin{itemize}          
      \item PVs area (m²)          
      \item Batteries capacity (kWh)                  
      \item Carbon emissions 
      \item Schedule of the workload         
      \end{itemize}
    \end{column}
  \end{columns}      
\end{frame}


\begin{frame}{ Previous work (some results) }
  
  \begin{table}[!ht]
    \caption{Total emissions of 1 year operation for the different scenarios.}\label{tab:emissions} \centering
    \begin{tabular}{|p{5cm}|r|}
      \hline
      \textbf{Scenarios} & \textbf{Emissions (t \ch{CO2}-eq)}   \\
      \hline
      Local electrical grid                    &   201211.3     \\
      \hline
      DC renewable infra   &  42370.6      \\ 
      \hline
      Hybrid: dc renewable   and grid             &  29600.6       \\
      \hline
    \end{tabular}
  \end{table}

  Reductions on carbon emissions:
  
  \begin{itemize}
  \item Grid vs DC renewable infra : \alert{$\simeq 5$ times}
  \item Grid vs hybrid configuration (DC renewables and grid) : \alert{$\simeq 6$ times}
  \end{itemize}
  
\end{frame}


\begin{frame}{Plan for today's presentation}

  Preliminary results regarding ongoing work:

  \begin{itemize}
  \item Studying sensitivity of the model
    \begin{itemize}
    \item  intermittency of solar irradiance 
    \end{itemize}
  \item Other strategies to minimize the \ch{CO2} emissions
    \begin{itemize}
    \item Wind power
    \item Flexibility in the scheduling
    \end{itemize}
  \item How expensive is the renewable infrastructure ?
  \item Extending the model to the long term
    \begin{itemize}
    \item When to add/replace servers considering \ch{CO2} from manufacturing
    \end{itemize}
  \end{itemize}

\end{frame}

\section{Preliminary results}


\begin{frame}{Solar irradiance intermittency}

  
  \begin{itemize}    
  \item What is the impact in the sizing of using only 1 year of irradiation?      
  \end{itemize}      
  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=.8\textwidth]{images/pv_boxplots_1990_2019.pdf}
      \caption{PV sizing using irradiation from years 1990 to 2019.}
    \end{figure}
  \end{center}
\end{frame}


\begin{frame}{Solar irradiance intermittency - which data to use ?}

  \begin{itemize}
  \item Average or median are enough ?
    \begin{itemize}
    \item Average and median of the 30 years => 1 year of irradiation
    \item Run the sizing for the pv area and batteries capacity
    \end{itemize}

  \item Evaluation   
    \begin{itemize}
    \item Use the computed sizing (avg and median) for the DC
      operation of the next 10 years ( 2010 - 2019 )
    \end{itemize}

    
  \item Baseline
    \begin{itemize}
    \item Optimal sizing for the period 2010 - 2019 
    \item All information of irradiation is know in advance
    \end{itemize}
    \pause
    \begin{table}
      
      \caption{Emissions (in kt \ch{CO2}-eq) for the different scenarios} \centering

      \begin{tabular}{|l|r|r|}
        
        \hline
        
        \textbf{Sizing Scenario} &  \textbf{Emissions } & \textbf{Diff. to baseline (\%) } \\
        \hline
        
        Optimal sizing 2010 - 2019  &       296.367 & - \\
        \hline     

        Average irrad.  1980 - 2009  & 301.237 &  1.62 \\
        \hline

        Median irrad. 1980 - 2009  &        304.489 & 2.66 \\
        \hline 
        
      \end{tabular}  
    \end{table}
    
    \footnotesize
    
    \textit{Obs: In this analysis all the other parameters are fixed (workload, emissions, DC and servers power consumption and so on) to isolate only the impact of the solar irradiation.}

  \end{itemize}
  
\end{frame}

\begin{frame}{Impact of adding wind power}

  Used for night computations and locations with low irradiance levels
  
  \pause      

  \small
  
  \begin{table}[H]

    \caption{Computed number of Wind Turbines (WT) for each location}\label{tab:results_wt} \centering
    
    \begin{tabular}{|l|r|}
      
      \hline

      \textbf{Location} &   \textbf{Number of WT} \\
      \hline
      Johannesburg & 7  \\
      \hline
      Pune         & 7  \\
      \hline
      Canberra     & 7  \\
      \hline
      Dubai        &  6 \\
      \hline
      Singapore    & 0  \\
      \hline     
      Seoul        & 26 \\
      \hline
      Virginia     & 0  \\
      \hline
      São Paulo    & 8  \\
      \hline 
      Paris        & 20 \\
      \hline  

    \end{tabular}  

  \end{table}

  \begin{itemize}
  \item \alert{Reduction of 6\%} of \ch{CO2} (1 year)  in comparison to PV and batteries only
  \item However, some locations may not be adequate for deploying the WT (Paris for example)
  \end{itemize}
  
\end{frame}

\begin{frame}{Flexibility in the scheduling}
  
  What is the impact in carbon emissions of delaying $\alpha$ percent of the jobs up to $\beta$ time slots (1h per time slot) ?
  \pause

  \begin{table}[H]
    \caption{Initial results regarding total carbon emissions ( tons of \ch{CO2} eq) for different values of $\alpha$ and $\beta$)}\centering
    \begin{tabular}{|l|r|r|r|r|r|r|r|r|r|r|}
      \hline
      \textcolor{blue}{$\alpha$}, \textcolor{red}{$\beta$} &   \textcolor{red}{\textbf{$ 1 h $}} &   \textcolor{red}{\textbf{$ 6 h $}} &  \textcolor{red}{\textbf{$ 12 h $}} &  \textcolor{red}{\textbf{$ 24 h $}} &  \textcolor{red}{\textbf{$ 48 h $}}  &   \textcolor{red}{\textbf{$ 96 h$}} &   \textcolor{red}{\textbf{$ 120 h $}} &   \textcolor{red}{\textbf{$ 144 h$}} &   \textcolor{red}{\textbf{$ 168 h$}} \\ 
      \hline
      \textcolor{blue}{ \textbf{10.0 \%}}   &  0.08 &  0.3 &  0.43 &  0.59 &  0.88 &  1.15 &  1.27 &  1.36 &  1.42 \\ 
      \hline
      \textcolor{blue}{ \textbf{20.0 \%}}   &  0.14 &  0.43 &  0.59 &  0.88 &  1.16 &  1.52 &  1.55 &  1.55 &  1.55 \\ 
      \hline
      \textcolor{blue}{ \textbf{30.0 \%}}   &  0.19 &  0.51 &  0.75 &  1.04 &  1.38 &  1.55 &  1.55 &  1.56 &  1.56 \\ 
      \hline
      \textcolor{blue}{ \textbf{40.0 \%}}   &  0.24 &  0.59 &  0.88 &  1.16 &  1.52 &  1.56 &  1.56 &  1.57 &  1.57 \\ 
      \hline
      \textcolor{blue}{ \textbf{50.0 \%}}   &  0.27 &  0.67 &  0.96 &  1.28 &  1.55 &  1.56 &  1.57 &  1.57 &  1.57 \\ 
      \hline
    \end{tabular}
  \end{table}

  \footnotesize
  \textit{Obs: In this analysis all the parameters are fixed (workload, emissions, DC and servers power consumption, irradiation and so on) to isolate only the impact of the different scheduling policy.}
  
\end{frame}


\begin{frame}{Costs (\$) of reducing the environmental impact}
  
  \begin{itemize}
    
  \item Battery: \$ 15.3 per kWh per year (lifetime of 10 years)
  \item PV: \$ 6.66 per m² per year ( lifetime of 30 years)
  \item WT: \$ 139500 per WT per year (lifetime of 20 years)
    
  \end{itemize}

  \begin{table}[H]
    
    \caption{Local grid electricity price (\$ per kWh) for each location. Source: \url{ https://www.globalpetrolprices.com/electricity_prices/}}\label{tab:grid_price} \centering

    \begin{tabular}{|l|r|}
      \hline
      
      \textbf{Location} &   \textbf{Price} \\
      \hline
      Johannesburg & 0.074   \\
      \hline
      Pune  & 0.104 \\
      \hline
      Canberra  & 0.331 \\
      \hline
      Dubai   &  0.110\\
      \hline
      Singapore & 0.272  \\
      \hline     
      Seoul    & 0.092  \\
      \hline
      Virginia   & 0.150 \\
      \hline
      São Paulo   & 0.144 \\
      \hline 
      Paris    &    0.340 \\
      
      \hline  

    \end{tabular}  
  \end{table}
\end{frame}



\begin{frame}{Costs (\$) and \ch{CO2} emissions}
  \small
  \begin{table}[H]
    \caption{Total costs of 1 year's operation  (millions of \$) for different scenarios }\label{tab:total_price} \centering
    \begin{tabular}{|l|r|}
      \hline
      
      \textbf{Scenario} &   \textbf{Total costs (millions of \$)} \\
      \hline
      Minimum cost (PV + Bat + WT + grid) & 36.926   \\
      \hline
      Minimum \ch{CO2} (PV + Bat + WT + grid)  & 45.667 \\
      \hline
      Only grid   & 75.378   \\
      \hline
    \end{tabular}  
  \end{table}

  \pause
  \begin{table}[H]
    
    \caption{Total emissions  of 1 year's operation (tons of CO2) for different scenarios }\label{tab:total_co2_scenarios} \centering
    
    \begin{tabular}{|l|r|}
      \hline
      
      \textbf{Scenario} &   \textbf{Total CO2 emissions (tons)} \\
      \hline
      Minimum \ch{CO2} (PV + Bat + WT + grid)  & 27821.1\\
      \hline
      Minimum cost (PV + Bat + WT + grid) &  101170.9\\
      \hline
      Only grid & 201211.3 \\
      \hline


    \end{tabular}  
  \end{table}
  \pause
  \begin{itemize}
  \item More than \alert{50\%} of reduction in costs (\$) in comparison to using only grid
  \item However, the minimum cost solution emits \alert{3 times} more than the minimum \ch{CO2} solution
  \end{itemize}

\end{frame}



\begin{frame}{When to add/replace servers given their environmental impact}

  \begin{itemize}
  \item Servers are more powerfull at each generation (CPU frequency, number of cores) and can be more power-efficient
  \item Workload keep increasing over the years
  \item Manufacturing servers also emits carbon
  \end{itemize}

  \small
  \begin{table}[H]
    
    \caption{Specifications of different servers generations. Source: SPECpower. } \centering
    \label{tab:servers_specs} 
    \begin{tabular}{|l|r|r|r|r|}
      \hline
      
      \textbf{Gen.} & \textbf{\# CPU Cores}& \textbf{P idle}  & \textbf{P Core} & \textbf{Nodes} \\
      \hline
      1& 20 & 52.0 & 7.5 & 1 \\
      \hline
      2 & 36 & 39.0 & 6.4 & 1  \\
      \hline
      3 &  36 & 40.1 & 6.3 & 1 \\
      \hline
      4 &  40 & 43.5 & 7.1 & 1 \\
      \hline
      5 & 56 & 48.9 & 6.7 & 1 \\
      \hline
      6 & 56 & 50.3 & 6.9 & 1 \\
      \hline
      7 & 64 & 66.1 & 2.7 & 1 \\
      \hline
      8 & 384 & 228.0 & 2.4 & 6 \\
      \hline
      9 & 128 & 75.6 & 3.0 & 1 \\
      \hline
      10 & 192 & 126.0 & 3.7 & 1 \\
      \hline
    \end{tabular}  
  \end{table}

\end{frame}


\begin{frame}{When to add/replace servers given their environmental impact}

  \alert{Assumptions:}
  
  \begin{itemize}

  \item Workload increase 10\% per year
  \item Manufacturing a new server emits 800 kg of \ch{CO2} eq
    \begin{itemize}
    \item Server's life time: 4 years
    \end{itemize}
  \item At each year, the decision is made to replace or add servers of the new generation
  \end{itemize}
  \pause
  \alert{Evaluation:}
  \begin{itemize}
  \item Optimal scenario where all the information about the servers, workload, climate condition is know in advance (10 years)
  \end{itemize}
\end{frame}


\begin{frame}{When to add/replace servers given their environmental impact}

  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2014.png}
      \caption{Initial data center configuration}
    \end{figure}
  \end{center}

\end{frame}



\begin{frame}{When to add/replace servers given their environmental impact}


  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2015.png}
      \caption{Data center configuration after considering the servers of the generation 2}
    \end{figure}
  \end{center}

  
\end{frame}



\begin{frame}{When to add/replace servers given their environmental impact}


  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2016.png}
      \caption{Data center configuration after considering the servers of the generation 3}
    \end{figure}
  \end{center}

  
\end{frame}



\begin{frame}{When to add/replace servers given their environmental impact}


  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2017.png}
      \caption{Data center configuration after considering the servers of the generation 4}
    \end{figure}
  \end{center}

  
\end{frame}

\begin{frame}{When to add/replace servers given their environmental impact}


  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2018.png}
      \caption{Data center configuration after considering the servers of the generation 5}
    \end{figure}
  \end{center}
  
\end{frame}


\begin{frame}{When to add/replace servers given their environmental impact}


  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2019.png}
      \caption{Data center configuration after considering the servers of the generation 6}
    \end{figure}
  \end{center}
  
\end{frame}


\begin{frame}{When to add/replace servers given their environmental impact}

  \begin{center}
    
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2020.png}
      \caption{Data center configuration after considering the servers of the generation 7}
    \end{figure}
  \end{center}

  
\end{frame}

\begin{frame}{When to add/replace servers given their environmental impact}

  \begin{center}

    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2021.png}
      \caption{Data center configuration after considering the servers of the generation 8}
    \end{figure}
  \end{center}
  
\end{frame}

\begin{frame}{When to add/replace servers given their environmental impact}


  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2022.png}
      \caption{Data center configuration after considering the servers of the generation 9}
    \end{figure}
  \end{center}
  
\end{frame}

\begin{frame}{When to add/replace servers given their environmental impact}

  \begin{center}
    
    \begin{figure}[!h]
      \centering
      \includegraphics[width=\textwidth]{images/final_dc_2023.png}
      \caption{Data center configuration after considering the servers of the generation 10}
    \end{figure}
  \end{center}
  
\end{frame}



\begin{frame}{When to add/replace servers given their environmental impact}

  \begin{columns}[T]
    \begin{column}{0.5\textwidth}
      
      \begin{figure}[!h]
        \centering
        \includegraphics[width=\textwidth]{images/final_dc_2023.png}
        \caption{Final DC - adding/replacing servers year by year}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}
      \begin{figure}[!h]
        \centering
        \includegraphics[width=\textwidth]{images/final_dc_2023_optimal.png}
        \caption{Optimal final DC (all information known in advance)}
      \end{figure}

    \end{column}
  \end{columns}
  
  \begin{itemize}

  \item \alert{8\%} difference in total carbon emissions
  \item Servers are used for longer in the baseline, and less modifications between the generations (it waits for the best server generations)
  \end{itemize}

\end{frame}

\section{Conclusion and future work}



\begin{frame}{Conclusion}
  \alert{Linear program to reduce the \ch{CO2} of operating a cloud federation}
  \begin{itemize}        
  \item Previous work: short term (1 year operation)

  \item Ongoing work:
    
  \item Studying the sensibility of the model to intermittency
    
    \begin{itemize}
      
    \item Simple methods are efficient (avg. or median of irradiation)

    \end{itemize}

  \item Other strategies to reduce the \ch{CO2} emissions

    \begin{itemize}
      
    \item Wind power: 6\% of reduction
    \item Delaying workload: up to 1.57\% of reduction
      
    \end{itemize}
    
  \item When to add/replace servers

    \begin{itemize}
      
    \item Year by year solution 8\% worse than the optimal

      
    \end{itemize}
    
    
  \end{itemize}
  
\end{frame}



\begin{frame}{Future work}

  Sizing for the long term :

  \begin{itemize}
    
  \item What could be learned from the optimal solution of adding/replacing servers ?
  \item Costs of the servers     
  \item Flexibility in the scheduling to reduce the number of
    servers manufactured
  \item Degradation of the renewable infrastructure over the years
    
  \end{itemize}    
\end{frame}

\begin{frame}{Sponsor acknowledgments}
  
  This work has been partially supported by the LabEx PERSYVAL-Lab (``ANR-11-LABX-0025-01'') funded by the French program \textit{Investissement d'avenir}, by grant \#2021/06867-2, São Paulo Research Foundation (FAPESP), by the EIPHI Graduate school (contract ``ANR-17-EURE-0002''), by the EuroHPC EU Regale project (g.a. 956560), and by the ANR DATAZERO2 (contract ``ANR-19-CE25-0016'') project.
\end{frame}

\begin{frame}{Thank you!}

  \textbf{Long-term evaluation for sizing low-carbon cloud data centers}

  \begin{center}
    \begin{figure}
      \centering      
      \includegraphics[width=0.4\textwidth]{images/qr-code.png}      
      \caption{Reproducible artifact: https://doi.org/10.25666/dataubfc-2023-02-03  }
    \end{figure}
    
    
    \textbf{Contact:}
    
    \alert{miguel.silva-vasconcelos@inria.fr}

  \end{center}

\end{frame}

\appendix

\begin{frame}

  Modeling wind speed:

  \begin{equation} \label{eq:wt_power_prod}
    Pwt^d_{k} = WT^d \times \left\{ 
      \begin{array}{ c l }
        0   & \quad \textrm{if } V^d_k \leq Vici \\
        Pr \times \frac{V^d_k - Vici}{Vr - Vici}  & \quad \textrm{if } Vici < V^d_k \leq Vr \\
        Pr  & \quad \textrm{if } Vr < V^d_k \leq Vco \\
        0  & \quad \textrm{if } Vco < V^d_k \\
      \end{array}
    \right.
  \end{equation}

  where \alert{$V^d_k$} is the wind speed at location of data center $d$ at instant $k$; \alert{$Vici$} is the \textit{cut in} wind speed, that is, the wind turbines start producing power when the wind speed is greater or equal to $Vici$; \alert{$Vco$} is the \textit{cut out} wind speed, that is, the wind turbines stop producing power when the wind speed is greater than $Vco$; \alert{$Pr$} is the WT rated power production in W; and \alert{$Vr$} is the speed where the WT starts producing \alert{$Pr$} power. 

\end{frame}

\end{document}
